Source: python-lib25519
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Simon Josefsson <simon@josefsson.org>,
 Jan Mojžíš <janmojzis@debian.org>,
Priority: optional
Standards-Version: 4.7.0
Section: python
Homepage: https://github.com/janmojzis/python-lib25519
Build-Depends:
 debhelper-compat (= 13),
 lib25519-1,
 pybuild-plugin-pyproject,
 python3-all,
 python3-pytest,
 python3-setuptools,
Vcs-Git: https://salsa.debian.org/python-team/packages/python-lib25519.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-lib25519
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild

Package: python3-lib25519
Architecture: all
Depends:
 lib25519-1,
 ${misc:Depends},
 ${python3:Depends},
Description: microlibrary X25519/Ed25519 - Python wrapper
 lib25519 is a microlibrary for the X25519 encryption system and the Ed25519
 signature system, both of which use the Curve25519 elliptic curve.
 .
 lib25519 has a very simple stateless API based on the SUPERCOP API, with
 wire-format inputs and outputs, providing functions that directly match the
 central cryptographic operations in X25519 and Ed25519:
 .
 lib25519.x25519.keypair(pk, sk): X25519 key generation
 lib25519.x25519.dh(k, pk, sk): shared-secret generation
 lib25519.ed25519.keypair(pk, sk): Ed25519 key generation
 lib25519.ed25519.sign(sm, &smlen, m, mlen, sk): signing
 lib25519.ed25519.open(m, &mlen, sm, smlen, pk): verification + message recovery
